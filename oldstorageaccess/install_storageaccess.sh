#!/bin/bash
if [ `whoami` != root ]; then
    echo "Please run this with sudo"
    exit
fi
#endpoint=$(hostname -f)
endpoint="localhost"
userid=$SUDO_USER
workdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
homedir="/home/$userid"
rclonf="$workdir/generatedrclone.conf"
echo "Is this a CloudBox install? If not sure choose 3 and check before installing."
select yn in "Yes" "No" "Exit"; do
    case $yn in
        Yes ) CB="true"; break;;
        No ) CB="false"; break;;
	Exit ) exit 0;;
    esac
done
echo "Is the server located in the US / Canada?"
select yn in "Yes" "No"; do
	case $yn in
        Yes ) US="true"; break;;
        No ) US="false"; break;;
    esac
done
echo "Rclone will be installed with vfs-cache-mode full and cache max-size at 100GB, do you want to increase cache-maxsize?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) echo "Please enter cache max size in digits+G (ex 200G)"; read vfssize; break;;
		No ) vfssize=100G; break;;
	esac
done
echo "Rclone logging is set to INFO, do you want to use DEBUG?"
select yn in "Yes" "No"; do
	case $yn in
		Yes ) rclonelog="DEBUG"; break;;
		No ) rclonelog="INFO"; break;;
	esac
done
echo "Please enter your Access Key:"
read key
echo "Please enter your Access Secret:"
read secret

echo "Please review the settings before continuing:"
if [ $CB = true ]; then
	echo "Cloudbox install will install mounts in /mnt"
fi
if [ $CB = false ]; then
	echo "Standard Debian / Ubuntu install, will install mounts in /home/user/"
fi
echo "local user:       $userid"
echo "Access Key:       $key"
echo "Access Secret:    $secret"
echo "Cloudbox install: $CB"
echo "US / Canada location:   $US"
echo "Rcone Cache size: $vfssize"
echo "Rclone loglevel:   $rclonelog"
echo "Continue with installation?"
select yn in "Continue" "Exit"; do
        case $yn in
                Continue ) break;;
                Exit ) exit 0;;
        esac
done

mkdir -p $homedir/.config/rclone
mkdir -p $homedir/logs
if [ $CB = true ]; then
	mkdir -p /mnt/storage
	chown -R $userid:$userid $homedir/.config/rclone $homedir/logs /mnt/storage
else
	mkdir -p $homedir/storage
	chown -R $userid:$userid $homedir/.config/rclone $homedir/logs $homedir/storage
fi
sed -e "s/ACCESSID/$key/g" -e "s/ACCESSKEY/$secret/g" -e "s/ENDPO/$endpoint/g" "$workdir/rclone.conf.temp" > "$homedir/.config/rclone/storage.conf"
chown $userid:$userid "$homedir/.config/rclone/"

echo "Installing latest rclone"
### DO NOT UPDATE WITHOUT TESTING
wget https://downloads.rclone.org/v1.57.0/rclone-v1.57.0-linux-amd64.deb $workdir/
dpkg -i rclone-v1.57.0-linux-amd64.deb

echo "Installing NGINX via apt-get and loading loadbalancer config"
apt update && RUNLEVEL=1 apt install nginx fuse -y
unlink /etc/nginx/sites-enabled/default
sed -e "s/\#user_allow_other/user_allow_other/" "/etc/fuse.conf"
echo "Installing loadbalancer config and enabling it"
if [ $US = true ]; then
	cp "$workdir/loadbalancer.conf.temp" "/etc/nginx/sites-available/loadbalancer.conf"
	ln -s /etc/nginx/sites-available/loadbalancer.conf /etc/nginx/sites-enabled/
else
	cp "$workdir/hetzner.conf.temp" "/etc/nginx/sites-available/storage.conf"
	ln -s /etc/nginx/sites-available/storage.conf /etc/nginx/sites-enabled/
fi
service nginx restart

if [ $CB = true ]; then
	echo "Installing storage-mounts at /mnt/"
	sed -e "s|USERID|$userid|g" -e "s|CACHESIZE|$vfssize|g" -e "s|log-level=DEBUG|log-level=$rclonelog|g" -e "s|/home/$userid/storage|/mnt/storage|g" "$workdir/storage-mount.service.temp" > "/etc/systemd/system/storage-mount.service"
else
	echo "Installing storage-mounts at /home/$userid"
	sed -e "s|USERID|$userid|g" -e "s|CACHESIZE|$vfssize|g" -e "s|log-level=DEBUG|log-level=$rclonelog|g" "$workdir/storage-mount.service.temp" > "/etc/systemd/system/storage-mount.service"
fi

chmod 751 /etc/systemd/system/storage-mount.service
systemctl enable storage-mount.service

systemctl daemon-reload
echo "mounting storage"
sudo service storage-mount start
