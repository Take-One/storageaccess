# StorageAccess

This script will install TakeOne storage access on any debian / Ubuntu based system or a CloudB0x install. 
Script has been tested against clean Debian 10 and fresh CloudB0x install, while most should work there are some reported issues 
with installing nginx on cloudb0x as those use their own docker version. Some CLI knowledge is therefor required.

Script will install the following files:
- rclone config file with S3 secrets to standard rclone config location. (homedir/.config/rclone/rclone.conf)
- nginx config for loadbalancing (/etc/nginx/sites-available/storage.conf or us-loadbalancer.conf)
- systemd service file for mounting storage (/etc/systemd/system/storage-mount.service)

Aside from the above rclone v1.57.0 and nginx will be installed.

If, after running the script, you want to change the mount point of storage you'll need to change the systemd service file.

- storage:media /home/USERID/storage
- ExecStop=/bin/fusermount -uz /home/USERID/storage

In the mount there is a directory named zTemplate, this contains a copy of the latest Plex metadata and databases. Copy over the directories "Media" "Metadata" to the correct locations within the Plex directory. Copy the latest database with a date after it into plexpath/Plugin Support/Databases and rename the two files to com.plexapp.plugins.library.db and com.plexapp.plugins.library.blobs.db. 

In Plex make sure the internal path is /home/plex/media. If using Plex Docker image change the mapping to /mnt/mountpoint:/home/plex/media.

When all is in the right place and Plex is running properly on the latest database you'll need to do a full scan to get it up in sync with the latest additions on storage.

For autoscan you'll need to install https://github.com/Cloudbox/autoscan and open port 3030. 
You will also need to add rewrites to translate /home/plex/media sent by the backend to your local mountpoint.

Add two triggers:

  manual:
    priority: 1
  sonarr:
    - name: sonarr
      priority: 2
  radarr:
    - name: radarr
      priority: 2


After that let me know if it's up and i'll add your server to autoscan on my backend.
