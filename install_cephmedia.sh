#!/bin/bash
if [ `whoami` != root ]; then
    echo "Please run this with sudo"
    exit
fi
user=$SUDO_USER

echo "Please enter your Access Key:"
read key
echo "Please enter your Access Secret:"
read secret

echo "Please review the access secret  before continuing:"
echo "Access Key:       $key"
echo "Access Secret:    $secret"

select yn in "Continue" "Exit"; do
        case $yn in
                Continue ) break;;
                Exit ) exit 0;;
        esac
done

apt update
apt install xattr ceph-common -y

mkdir -p /etc/ceph/

echo '# minimal ceph.conf for f4d0156e-4eab-11ed-b24f-6cb311095688
[global]
        fsid = f4d0156e-4eab-11ed-b24f-6cb311095688
        mon_host = [v2:65.108.35.17:3300/0,v1:65.108.35.17:6789/0] [v2:65.108.132.50:3300/0,v1:65.108.132.50:6789/0] [v2:65.109.17.220:3300/0,v1:65.109.17.220:6789/0] [v2:65.109.17.221:3300/0,v1:65.109.17.221:6789/0] [v2:65.109.17.199:3300/0,v1:65.109.17.199:6789/0]' | sudo tee /etc/ceph/ceph.conf

echo $secret | sudo tee /etc/ceph/external.secret

echo '[client.'$key']
              key = '$secret'' | sudo tee /etc/ceph/ceph.client.$key.keyring

echo '#storage1.igu-ana.one:,storage2.igu-ana.one:,storage3.igu-ana.one:,storage4.igu-ana.one:,storage5.igu-ana.one:/ MOUNTPOINT ceph name='$key',secretfile=/etc/ceph/external.secret,rsize=67108864,rasize=67108864,noatime,_netdev  0 2' | sudo tee -a /etc/fstab

exit 0
